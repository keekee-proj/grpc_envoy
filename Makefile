WORKDIR=$(shell pwd)
LOCAL_BIN:=$(WORKDIR)/bin
GOPATH=$(shell go env GOPATH)
DOCKER_GOPATH=/go/src/experiments/grpc
DOCKER_IMAGE=grpc-test

PKGMAP:=Mgoogle/protobuf/any.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/api.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/descriptor.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/field_mask.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/source_context.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/struct.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/type.proto=github.com/gogo/protobuf/types,$\
        Mgoogle/protobuf/wrappers.proto=github.com/gogo/protobuf/types

gen:
	protoc -I$(WORKDIR)/api \
		-I$(WORKDIR)/vendor.pb \
		--go_out=$(PKGMAP),plugins=grpc:./pkg/grpc \
		--grpc-gateway_out=logtostderr=true,$(PKGMAP):./pkg/grpc \
		--swagger_out=logtostderr=true:./swagger \
		--include_imports --include_source_info \
		--descriptor_set_out=./api/descriptor.pb \
		service.proto

deps:
	go get -u github.com/golang/protobuf/protoc-gen-go
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options

build:
	docker build -t $(DOCKER_IMAGE) ./

up:
	docker run --rm -it $(DOCKER_IMAGE)

attach:
	docker run --rm -it -v $(WORKDIR):$(DOCKER_GOPATH) $(DOCKER_IMAGE) bash

attach.sh:
	docker run --rm -it $(DOCKER_IMAGE) sh

vendor.proto:
	go run cmd/proto_vendoring.go --in api/service.proto