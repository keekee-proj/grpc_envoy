package app

import (
	gw "bitbucket.org/keekee-proj/grpc_envoy/pkg/grpc"
	"context"
)

type Server struct {
}

func (s *Server) Check(context.Context, *gw.Empty) (*gw.Empty, error) {
	return new(gw.Empty), nil
}
func (s *Server) Ping(context.Context, *gw.Empty) (*gw.Empty, error) {
	return new(gw.Empty), nil
}
