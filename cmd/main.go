package main

import (
	"bitbucket.org/keekee-proj/grpc_envoy/internal/app"
	test_client "bitbucket.org/keekee-proj/grpc_envoy/pkg/grpc"
	"context"
	"flag"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var (
	addrGrpc = flag.String("addr_grpc", ":8890", "listening address")
	addrHttp = flag.String("addr_http", ":8900", "listening address")
)

func mainGw() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := test_client.RegisterExpServiceHandlerFromEndpoint(ctx, mux, *addrGrpc, opts)
	if err != nil {
		log.Fatal(err)
	}

	err = http.ListenAndServe(*addrHttp, mux)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	lis, err := net.Listen("tcp", *addrGrpc)
	if err != nil {
		log.Fatal(err)
	}
	serv := grpc.NewServer()
	test_client.RegisterExpServiceServer(serv, new(app.Server))

	notifyCh := make(chan os.Signal)
	signal.Notify(notifyCh, syscall.SIGTERM, syscall.SIGINT)
	log.Println("start serv")
	go func() {
		err = serv.Serve(lis)
		if err != nil {
			log.Println(err)
		}
	}()
	<-notifyCh
	log.Println("stopping server")
	serv.GracefulStop()
	log.Println("done")
}
