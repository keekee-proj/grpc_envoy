package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/fatih/color"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var (
	in  = flag.String("in", "", "")
	out = flag.String("out", "vendor.pb", "")
)

var importRegexp = regexp.MustCompile(`^import "(.*?)";$`)
var separator = string(filepath.Separator)

var knownImports = map[string]struct{}{}

func main() {
	flag.Parse()
	protoAbsPath, _ := filepath.Abs(*in)
	protoExt := filepath.Ext(*in)
	if protoExt != ".proto" {
		log.Fatalf("not proto")
	}
	if _, err := os.Stat(protoAbsPath); !os.IsNotExist(err) {

	}
	if err := getDeps(); err != nil {
		log.Fatal(err)
	}
	generateImports(protoAbsPath, *out, 1)
}

func generateImports(inProto, outDir string, lvl int) {
	protoFile, err := os.Open(inProto)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(protoFile)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if m := importRegexp.FindStringSubmatch(scanner.Text()); len(m) > 1 {
			// proto - example: github.com/name/project/v2/package/subpackage/service.proto
			proto := m[1]
			name := filepath.Base(proto)

			log.Printf("Found proto import: %s\n", proto)

			if _, ok := knownImports[proto]; ok {
				log.Println("Import already processed: skip")
				continue
			}
			knownImports[proto] = struct{}{}

			// protoImport example: github.com/name/project/v2/package/subpackage
			protoImport := strings.TrimSuffix(proto, separator+name)
			pkgParts := strings.Split(protoImport, separator)
			log.Printf("proto import package: %s\n", protoImport)

			var repoFound bool
			var dependency dep
			var repoPkg string

			for repoPartsCount := 1; !repoFound && repoPartsCount <= len(pkgParts); repoPartsCount++ {
				// repo example: github.com/name/project[/v2]
				repo := strings.Join(pkgParts[:repoPartsCount], separator)
				if len(pkgParts) > repoPartsCount && strings.HasPrefix(pkgParts[repoPartsCount], "v") {
					if _, err := strconv.Atoi(strings.TrimPrefix(pkgParts[repoPartsCount], "v")); err == nil {
						repo += separator + pkgParts[repoPartsCount]
					}
				}
				log.Printf("try repo: %s\n", repo)

				// repoPkg example: package/subpackage
				repoPkg = strings.TrimPrefix(strings.TrimPrefix(protoImport, repo), separator)
				log.Printf("try repo package: %s\n", repoPkg)

				if rRepo, rRepoPkg, replaced := replaceRepo(repo); replaced {
					repo = rRepo
					repoPkg = rRepoPkg
					log.Printf("repo replaced: %s\n", repo)
					log.Printf("repo package replaced: %s\n", repoPkg)
				}

				dependency, repoFound = deps[repo]
				if !repoFound || dependency.Dir == "" {
					depVer := "latest"
					if dependency.Version != "" {
						depVer = dependency.Version
					}
					if err := getDeps(repo + "@" + depVer); err != nil {
						log.Printf("could not find repo: %s\n", repo)
						continue
					}
					dependency, repoFound = deps[repo]
				}
			}
			if repoFound {
				log.Printf("found repo: %s", dependency.Path)
				log.Printf("copy proto: %s -> %s\n", filepath.Join(dependency.Dir, repoPkg, name), filepath.Join(outDir, protoImport, name))
				if err := os.MkdirAll(filepath.Join(outDir, protoImport), os.ModePerm); err != nil {
					log.Fatalf("[ERROR] could not create dir `%s`: %s\n", filepath.Join(outDir, protoImport), err)
				}
				f, err := os.Open(filepath.Join(dependency.Dir, repoPkg, name))
				if err == nil {
					writeToFile(filepath.Join(outDir, protoImport, name), f)
					log.Println()
					generateImports(filepath.Join(outDir, protoImport, name), outDir, lvl+1)
				} else {
					log.Fatalf("[ERROR] failed to open file %s: %s\n", filepath.Join(dependency.Dir, repoPkg, name), err)
				}
			} else {
				log.Fatalf("[ERROR] could not find repo for proto file: %s\n", proto)
			}
		}
	}
	if err := scanner.Err(); err != nil {
		fmt.Println(color.RedString("failed to read %s:", inProto), err.Error())
	}
}

func replaceRepo(repo string) (replacedRepo, pkg string, replaced bool) {
	switch repo {
	case "google/type":
		return "github.com/googleapis/googleapis", "google/type", true
	case "google/api":
		return "github.com/googleapis/googleapis", "google/api", true
	case "google/rpc":
		return "github.com/googleapis/googleapis", "google/rpc", true
	case "google/protobuf":
		return "github.com/google/protobuf", "src/google/protobuf", true
	case "protoc-gen-swagger/options":
		return "github.com/grpc-ecosystem/grpc-gateway", "protoc-gen-swagger/options", true
	default:
		return "", "", false
	}
}

type dep struct {
	Path    string
	Dir     string
	Version string
}

var deps map[string]dep

func scanJsonObject(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.Index(data, []byte{'}', '\n'}); i >= 0 {
		return i + 2, data[0 : i+1], nil
	}
	if atEOF {
		return len(data), data, nil
	}
	return 0, nil, nil
}

func getDeps(get ...string) error {
	for _, g := range get {
		args := []string{"go", "get", g}
		cmd := exec.Command(args[0], args[1:]...)
		err := cmd.Run()
		if err != nil {
			return err
		}
	}

	args := []string{"go", "list", "-m", "-json", "all"}
	cmd := exec.Command(args[0], args[1:]...)
	out, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	err = cmd.Start()
	if err != nil {
		return err
	}
	scanner := bufio.NewScanner(out)
	scanner.Split(scanJsonObject)
	deps = make(map[string]dep)
	for scanner.Scan() {
		d := dep{}
		if err := json.Unmarshal(scanner.Bytes(), &d); err != nil {
			return err
		}
		deps[d.Path] = d
	}
	if err := scanner.Err(); err != nil {
		fmt.Println(color.RedString("failed to read result from `%s`:", strings.Join(args, " ")), err.Error())
	}
	return nil
}

func writeToFile(path string, r io.Reader) error {
	dir := filepath.Dir(path)
	if dir != "" {
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return err
		}
	}

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, r)
	return err
}
