FROM golang:1.11

COPY ./ /go/src/experiments/grpc
WORKDIR /go/src/experiments/grpc

RUN apt-get update && apt-get install -y make curl bash unzip jq && \
    ./protoc_install.sh && make deps
ENV GO111MODULE=on
RUN go mod vendor && make vendor.proto && make gen
EXPOSE 8890 8900

CMD go run cmd/main.go