#! /bin/sh

TEMP_DIR=/tmp

VERSION=3.7.1

mkdir -p $TEMP_DIR/protoc_inst/protoc3

cd $TEMP_DIR/protoc_inst

# Make sure you grab the latest version
curl -OL "https://github.com/protocolbuffers/protobuf/releases/download/v3.7.1/protoc-3.7.1-linux-x86_64.zip"

# Unzip
unzip "protoc-$VERSION-linux-x86_64.zip" -d protoc3

# Move protoc to /usr/local/bin/
mv protoc3/bin/* /usr/local/bin/

# Move protoc3/include to /usr/local/include/
mv protoc3/include/* /usr/local/include/

rm -rf $TEMP_DIR/protoc_inst